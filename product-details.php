<?php include_once 'sec/users_only.php'; ?>
<?php require_once 'class/Helper.class.php'; ?>
<?php require_once 'class/Product.class.php'; ?>

<?php




if( !isset($_GET['id']) ) {
  Helper::addError("The product ID is missing.");
  header("Location: ./products.php");
  die();
}

$product = new Product($_GET['id']);

if( !$product->created_at || $product->deleted_at ) {
  Helper::addError("Product not found.");
  header("Location: ./products.php");
  die();
}

if(isset($_POST['btn_ToDelete'])){
       $deleteProduct = new Product($_POST['product_id']);
       $delete_result = $deleteProduct->delete();
        if( $delete_result ){
            header("Location: ./products.php");
        }
    }


if( isset($_POST['btn_addToCart']) ) {
  $addedToCart = $product->addProduct($_POST['quantity']);
  if( $addedToCart ) {
    Helper::addMessage('Product added to cart successfully.');
  } else {
    Helper::addError('Failed to add product to cart.');
  }
}





?>

<?php include 'inc/header.inc.php'; ?>

<h1 class="my-5"><?php echo $product->title; ?></h1>

<div class="row">

  <div class="col-md-5">slika...</div>

  <div class="col-md-7">
    <h3 class="mb-5">Description</h3>
    <p class="mb-5">
    <?php echo $product->description; ?>
    </p>

    <div class="d-flex flex-column align-items-end">
      <h5 class="mt-5"><?php echo $product->price; ?> RSD</h5>

      <form action="" method="post">
        <div class="input-group mt-3">
          <input type="number" name="quantity" class="form-control" value="1" min="1" />
          <div class="input-group-append">
            <button name="btn_addToCart" class="btn btn-outline-primary">Add to cart</button>
          </div>
        </div>
      </form>
    </div>
  </div>
        <?php if( $loggedInUser->acc_type == "admin" ) { ?>
                <div class="input-group mt-3">
                    <form action="" method="post">   
                  <input type="hidden" name="product_id" value="<?php echo $product->id; ?>" />
                  <button name="btn_ToDelete" class="btn btn-outline-primary">Delete</button>
                  
                  <a href="./updateProduct.php?id=<?php echo $product->id; ?>" class="btn btn-primary">Update product</a>
                    </form>
                </div>
        <?php  } ?>
</div>


<?php include 'inc/footer.inc.php'; ?>
