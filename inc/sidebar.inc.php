<?php
        require_once 'class/Category.class.php';
        $c = new Category();
        $categories = $c->all();
?>
<h2 class="my-5">Sidebar</h2>

<?php foreach($categories as $category) { ?>
    <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
        <?php echo $category->title; ?>
      <span class="badge badge-primary badge-pill">#</span>
    </a>
  <?php } ?>