<?php

require_once 'class/User.class.php';

$loggedInUser = new User();
$loggedInUser->loadLoggedInUser();

?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <a class="navbar-brand" href="./index.php">WebShop</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="./index.php">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./products.php">Products</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./contact.php">Contact</a>
        </li>
      </ul>

      <ul class="navbar-nav">

      <?php if(User::isUserLoggedIn()) { ?>
          <li class="nav-item">
          <a href="./cart.php" class="nav-link">
              Cart
          </a>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php echo $loggedInUser->username; ?>
            (<?php echo $loggedInUser->email; ?>)
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="update_profile.php">Update profile</a>

            <?php if( $loggedInUser->acc_type == "admin" ) { ?>
              <h6 class="dropdown-header">Administration</h6>
              <a class="dropdown-item" href="./categories.php">Manage categories</a>
              <a class="dropdown-item" href="./addProduct.php">Add Product</a>
            <?php } ?>
            
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="./logout.php">Log out</a>
          </div>
        </li>
      <?php } else { ?>
        <li class="nav-item">
          <a href="./login.php" class="nav-link">Login</a>
        </li>
        <li class="nav-item">
          <a href="./register.php" class="nav-link">Register</a>
        </li>
      <?php } ?>

      </ul>
    </div>

  </div>
</nav>
