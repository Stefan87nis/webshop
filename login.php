<?php
require_once 'class/User.class.php';



if(isset($_POST['user_login'])){
    $addUser = new User();
    $addUser->email = $_POST['email'];
    $addUser->password = $_POST['password'];
    if( $addUser->login() ) {
    Helper::addMessage("Wellcome!");
  }
}

if( User::isUserLoggedIn() ) {
  header("Location: ./index.php");
  die();
}


include_once 'inc/header.inc.php';
?>

<h1 class="my-5">Registration page</h1>


<form action="login.php" method="post">
  <div class="form-group">
    <label for="inputEmail">Email</label>
    <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Enter Email address">
  </div>
  <div class="form-group">
    <label for="inputPassword">Password</label>
    <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Enter Password">
  </div>
  <button type="submit" name="user_login" class="btn btn-primary">Login</button>
</form>


<?php include_once 'inc/footer.inc.php'; ?>