<?php
require_once 'class/User.class.php';
require_once 'class/Helper.class.php';

if(isset($_POST['user_register'])){
    $addUser = new User();
    $addUser->name = $_POST['name'];
    $addUser->username = $_POST['username'];
    $addUser->email = $_POST['email'];
    $addUser->password = $_POST['password'];
    $addUser->password_repeat = $_POST['password_repeat'];

    if( $addUser->insertUser() ) {
    Helper::addMessage("Registration successfull!");
  }

}


include_once 'inc/header.inc.php';
?>

<h1 class="my-5">Registration page</h1>


<form action="register.php" method="post">
  <div class="form-group">
    <label for="inputName">Name</label>
    <input type="text" name="name" class="form-control" id="inputName"  placeholder="Enter Name">
  </div>
  <div class="form-group">
    <label for="inputUsername">Username</label>
    <input type="text" name="username" class="form-control" id="inputUsername" placeholder="Enter Username">
  </div>
  <div class="form-group">
    <label for="inputEmail">Email</label>
    <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Enter Email address">
  </div>
  <div class="form-group">
    <label for="inputPassword">Password</label>
    <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Enter Password">
  </div>
  <div class="form-group">
    <label for="inputPasswordAgain">Repeat password</label>
    <input type="password" name="password_repeat" class="form-control" id="inputPasswordAgain" placeholder="Repeat Password">
  </div>
  <button type="submit" name="user_register" class="btn btn-primary">Register</button>
</form>


<?php include_once 'inc/footer.inc.php'; ?>