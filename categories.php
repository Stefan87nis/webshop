<?php
    require 'sec/users_only.php';
    require 'sec/admin_only.php';
    require_once 'class/Category.class.php';

    
    
    if(isset($_POST['addCategory'])){
        $newCategory = new Category();
        $newCategory->title = $_POST['category_title'];
        if( $newCategory->insert() ) {
        Helper::addMessage("New category is added!");
    }
    }
    
    if(isset($_POST['delete'])){
        $deleteCategory = new Category($_POST['category_id']);
        $deleteCategory->delete();
    }
    
    
    $c = new Category();
    $categories = $c->all();
    
    
    
    include_once 'inc/header.inc.php';
?>

<h1 class="my-5">Add category</h1>

<form action="categories.php" method="post">
  <div class="form-group">
    <label for="inputCategory"></label>
    <input type="text" name="category_title" class="form-control" id="inputCategory"  placeholder="Enter category">
  </div>
  <button name="addCategory" type="submit" class="btn btn-primary">Add category</button>
</form>


<table class="table my-5">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Title</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      <?php   foreach($categories as $category){ ?>
    <tr>
      <td><?php echo $category->id; ?></td>
      <td><?php echo $category->title; ?></td>
      <td>
          <form action="" method="post">
          <input type="hidden" name="category_id" value="<?php echo $category->id; ?>" />
          <button name="delete"  class="btn btn-danger">DELETE</button>
        </form>
      </td>
    </tr>
      <?php } ?>
  </tbody>
</table>




<?php
    include_once 'inc/footer.inc.php';
?>