<?php

class Product {
  private $db;
  public $id;
  public $cat_id;
  public $title;
  public $description;
  public $price;
  public $img;
  public $created_at;
  public $deleted_at;
  
  function __construct($id = null) {
    $this->db = require 'db/db.php';
    require_once 'class/Helper.class.php';

    if($id) {
      $this->id = $id;
      $this->loadFromDb();
    }
  }

  public function loadFromDb() {
    $stmt_get = $this->db->prepare("
      SELECT *
      FROM `products`
      WHERE `id` = :id
    ");
    $stmt_get->execute([ ':id' => $this->id ]);

    $product = $stmt_get->fetch();

    if($product) {
      foreach($product as $key => $value) {
        $this->$key = $value;
      }
    }
  }

  
  public function insertTitleIsValid(){
      
        if( $this->title == "" ) {
        Helper::addError('Title is empty!');
        return false;
    }
    return true;
  }
  
  public function insertDescriptionIsValid(){
      
        if( $this->description == "" ) {
        Helper::addError('Description is empty!');
        return false;
    }
    return true;
  }
  public function insertPriceIsValid(){
      
        if( $this->price == "" ) {
        Helper::addError('Price is empty!');
        return false;
    }
    return true;
  }
  
  
  public function insert() {

      if( !$this->insertTitleIsValid() ) {
      return false;
    }   
    
    if( !$this->insertDescriptionIsValid()){
        return false;
    }
    
    if( !$this->insertPriceIsValid() ) {
      return false;
    } 
      //URADITI VALIDACIJU ZA INSERT
    $stmt_insert = $this->db->prepare("
      INSERT INTO `products`
      (`cat_id`, `title`, `description`, `price`)
      VALUES
      (:cat_id, :title, :description, :price)
    ");
    return $stmt_insert->execute([
      ':cat_id' => $this->cat_id,
      ':title' => $this->title,
      ':description' => $this->description,
      ':price' => $this->price
    ]);
  }

  public function titleIsValid(){
      
        if( $this->title == "" ) {
        Helper::addError('Title is empty!');
        return false;
    }
    return true;
  }
  
  public function descriptionIsValid(){
      
        if( $this->description == "" ) {
        Helper::addError('Description is empty!');
        return false;
    }
    return true;
  }
  public function priceIsValid(){
      
        if( $this->price == "" ) {
        Helper::addError('Price is empty!');
        return false;
    }
    return true;
  }
  
  
  public function update() {
      
     if( !$this->titleIsValid() ) {
      return false;
    }   
    
    if( !$this->descriptionIsValid()){
        return false;
    }
    
    if( !$this->priceIsValid() ) {
      return false;
    } 
      
      
      
    $stmt_update = $this->db->prepare("
      UPDATE `products`
      SET
        `cat_id` = :cat_id,
        `title` = :title,
        `description` = :description,
        `price` = :price
      WHERE `id` = :id
    ");

    return $stmt_update->execute([
      ':cat_id' => $this->cat_id,
      ':title' => $this->title,
      ':description' => $this->description,
      ':price' => $this->price,
      ':id' => $this->id
    ]);
  }

  public function delete() {
    $stmt_delete = $this->db->prepare("
      UPDATE `products`
      SET `deleted_at` = now()
      WHERE `id` = :id
    ");
    return $stmt_delete->execute([ ':id' => $this->id ]);
  }

  public function all() {
    $stmt_getAll = $this->db->prepare("
      SELECT *
      FROM `products`
      WHERE `deleted_at` IS NULL
    ");
    $stmt_getAll->execute();
    return $stmt_getAll->fetchAll();
  }
 
  public function addToCart($quantity) {
    require_once 'class/User.class.php';
    if( !User::isUserLoggedIn() ) {
      return false;
    }

    $stmt_addToCart = $this->db->prepare("
      INSERT INTO `carts`
      (`user_id`, `product_id`, `quantity`)
      VALUES
      (:user_id, :product_id, :quantity)
    ");

    Helper::sessionStart();

    return $stmt_addToCart->execute([
      ':user_id' => $_SESSION['user_id'],
      ':product_id' => $this->id,
      ':quantity' => $quantity
    ]);
  }

  
  
  public function addProduct($quantity){
      require_once 'class/User.class.php';
      if( !User::isUserLoggedIn()){
          return false;
      }
      
      
       $getUserProduct = $this->db->prepare("
      SELECT *
      FROM `carts`
      WHERE `user_id` = :user_id
      AND `product_id` = :product_id
        ");
        $getUserProduct->execute([
            ':user_id' => User::getLoggedInUserId(),
            ':product_id' => $this->id,
            ]);
        
        
    if ( $getUserProduct->rowCount() > 0 ) {
      $user_product_details = $getUserProduct->fetch();
      $new_quantity = $user_product_details->quantity + $quantity;
      return $this->updateQuantity($new_quantity);
    } else {
      $addToCart = $this->db->prepare("
        INSERT INTO `carts`
        (`user_id`, `product_id`, `quantity`)
        VALUES
        (:user_id, :product_id, :quantity)
      ");
      return $addToCart->execute([
          ':user_id' => User::getLoggedInUserId(),
          ':product_id' => $this->id,
          ':quantity' => $quantity
          ]);
    }
    }
    
    
     public function updateQuantity($newQuantity) {
    require_once 'class/User.class.php';

    $stmt_updateQuantity = $this->db->prepare("
      UPDATE `carts`
      SET `quantity` = :quantity
      WHERE `user_id` = :user_id
      AND `product_id` = :product_id
    ");
    return $stmt_updateQuantity->execute([
      ':quantity' => $newQuantity,
      ':user_id' => User::getLoggedInUserId(),
      ':product_id' => $this->id
    ]);
  }
  
  public function deleteCart(){
      $stmt_deleteCart = $this->db->prepare("
          DELETE  FROM `carts`
          WHERE `id` = :id
          ");
     return $stmt_deleteCart->execute([
          ':id' => $this->id
      ]);
      
  }
}