<?php

class User {
    
    private $db;
    public $id;
    public $name;
    public $username;
    public $email;
    public $password;
    public $password_repeat;
    public $acc_type;
    public $created_at;
    public $deleted_at;
    
    
    function __construct($id = NULL) {
        $this->db = require 'db/db.php';
        require_once 'class/Helper.class.php';
        
        if( $id ){
            $this->id = $id;
            $this->loadUserDb();
        }
    }
    
    public function loadUserDb(){
        $stmt_load_user = $this->db->prepare("
            SELECT * FROM `users`
            WHERE `id` = :id
            ");
        
        $stmt_load_user->execute([
            ':id' => $this->id
           ]);     
        
        $user = $stmt_load_user->fetch();
        if( $user ){
        $this->name = $user->name;
        $this->username = $user->username;
        $this->email = $user->email;
        $this->password = $user->password;
        $this->acc_type = $user->acc_type;
        $this->created_at = $user->created_at;
        $this->deleted_at = $user->deleted_at;
        }
    }
    
    public function passwordIsValid() {
    if( $this->password == "" ) {
        Helper::addError('Password is empty!');
      return false;
    }

    if( $this->password != $this->password_repeat ) {
        Helper::addError('Password not match!');
      return false;
    }

    return true;
  }

  public function nameIsValid() {
    if( $this->name == "" ) {
        Helper::addError('Name is empty!');
      return false;
    }

    return true;
  }

  public function emailIsValid() {
    if( $this->email == "" ) {
        Helper::addError('Email is empty!');
      return false;
    }

    if( !filter_var($this->email, FILTER_VALIDATE_EMAIL) ) {
        Helper::addError('Email format is invalid!!');
      return false;
    }
    
    return true;
  }

  public function usenameIsValid(){
      if($this->username == ""){
          Helper::addError('Username is empty!');
          return false;
      }
      return true;
  }
    

    
    public function insertUser(){
        
        
    if( !$this->nameIsValid() ) {
      return false;
    }   
    
    if( !$this->usenameIsValid()){
        return false;
    }
    
    if( !$this->emailIsValid() ) {
      return false;
    }

    if( !$this->passwordIsValid() ) {
      return false;
    }
    
        
        
        
        $stmt_insert_user = $this->db->prepare("
            INSERT INTO `users`
            (`name`,`username`,`email`,`password`)
            VALUES
            (:name, :username, :email, :password)
            ");
     $result =  $stmt_insert_user->execute([
            ':name' => $this->name,
            ':username' => $this->username,
            ':email' => $this->email,
            ':password' => md5($this->password)
            ]);
     
     if( $result ){
         $this->id = $this->db->lastInsertId();
         $this->loadUserDb();
     }
     return $result;
    }
    
    public function login() {
        $stmt_login_user = $this->db->prepare("
            SELECT * FROM `users`
            WHERE `email` = :email
            AND `password` = :password
            ");
        $stmt_login_user->execute([
            ':email' => $this->email,
            ':password' => md5($this->password)
        ]);
        
        
        $user = $stmt_login_user->fetch();
        
        
        if( $user ){
            Helper::addMessage("Login successfull!");
            Helper::sessionStart();
            $_SESSION['user_id'] = $user->id;
            return true;
        }
        Helper::addError('Invalid credentials.');
        return false;
        
    }
    
    public static function isUserLoggedIn(){
        require_once 'class/Helper.class.php';
        
        Helper::sessionStart();
        
        return isset($_SESSION['user_id']);
    }
    
    public static function getLoggedInUserId(){
        require_once 'class/Helper.class.php';
        
        Helper::sessionStart();
        
        if(isset($_SESSION['user_id'])){
            $_SESSION['user_id'];
            return true;
        }else{
        return false;
        }
    }
    
    public function loadLoggedInUser(){
        require_once 'class/Helper.class.php';

        Helper::sessionStart();
        
        
        if(isset($_SESSION['user_id'])){
            $this->id = $_SESSION['user_id'];
            $this->loadUserDb();
        }
    }
    
    public function update(){
            $stmt_update = $this->db->prepare("
            UPDATE `categories`
            SET 
            `name` = :name,
            `username` = :username,
            `email` = :email,
            `password` = :password,
            `acc_type` = :acc_type
            WHERE `id` = :id
            ");
      return $stmt_update->execute([
      ':name' => $this->name,
      ':username' => $this->username,    
      ':email' => $this->email,
      ':password' => $this->password,
      ':acc_type' => $this->acc_type,
      ':id' => $this->id
    ]);
      
  }
  
  public function getCart(){
    
      $stmt_getCart = $this->db->prepare("
          SELECT
          `carts`.`id`,
          `products`.`title`,
          `carts`.`quantity`,
          `products`.`price`
      FROM `carts`, `products`
      WHERE `carts`.`user_id` = :user_id
      AND `products`.`id` = `carts`.`product_id`
    ");
    $stmt_getCart->execute([
      ':user_id' => User::getLoggedInUserId()
    ]);
    return $stmt_getCart->fetchAll();
  }

}