<?php


class Helper {
    
    
    public static function sessionStart(){
        
        if( !isset($_SESSION )){
            session_start();
        }
    }
    //pomocu   staticke funkcije dodajemo gresku koju upotrebljavamo u validaciji 
    public static function addError($e){
        self::sessionStart(); //ovako se startuje staticka metoda koja je unutar iste klase
        $_SESSION['error'] = $e; //ovde definishemo $e tako sto sesija ima error za message
    }
    // odavde uzimamo gresku koju prikazujemo u addError
    public static function getError(){
        self::sessionStart();
        $e = NULL;
        //$e po defaultu je NULL, onda proveravamo da li postoji sesija $_SESSION['error'] 
        if(isset($_SESSION['error'])){
           $e = $_SESSION['error'];
           unset($_SESSION['error']);//unset čini nedefinisanom promenjljivu .
        }
        return $e;
    }
    
    public static function addMessage($m){
        self::sessionStart();
        $_SESSION['message'] = $m;
    }
    
    public static function getMessage(){
        self::sessionStart();
        $m = NULL;
        if(isset($_SESSION['message'])){
            $m = $_SESSION['message'];
            unset($_SESSION['message']);
        }
        return $m;
    }
}