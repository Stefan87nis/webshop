<?php 
 
require 'sec/users_only.php';
require_once 'class/Product.class.php';
$c = new Product();
$products = $c->all();




include_once 'inc/header.inc.php';
?>

<h1 class="my-5">Products</h1>


<div class="row">
    <?php foreach( $products as $product ) { ?>
<div class="col-md-4">
      <div class="card" >
        <img class="" type="file" name="img" src="" />
        <div class="card-body">
          <h5 class="card-title">
              <?php echo $product->title; ?>
          </h5>
          <p class="card-text">
            <strong>Price:<?php echo $product->price; ?></strong>
          </p>
          <div class="d-flex justify-content-end">
              <form action="" method="get">
                  <a href="./product-details.php?id=<?php echo $product->id; ?>" class="btn btn-primary">Details</a>
              </form>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
</div>

<?php include_once 'inc/footer.inc.php'; ?>