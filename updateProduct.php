<?php
    require 'sec/admin_only.php';
    require 'sec/users_only.php';
    require_once 'class/Product.class.php';
    require_once 'class/Helper.class.php';
    require_once 'class/User.class.php';
    
    
    
    $product = new Product($_GET['id']);
    
    
    if(isset($_POST['btn_addToUpdate'])){
        $updateProduct = new Product($_POST['product_id']);
        $updateProduct->title = $_POST['newTitle'];
        $updateProduct->description = $_POST['newDescription'];
        $updateProduct->price = $_POST['newPrice'];
        $update_result = $updateProduct->update();
        
        if( $update_result ){
            Helper::addMessage('Product is updated.');
        }
    }
    
    
    include_once 'inc/header.inc.php';
?>
        <form action="" method="post">
                <div class="form-group">  
                    <label for="inputNewTitle">New Title</label>
                    <input type="text" name="newTitle" class="form-control" id="inputNewTitle" placeholder="Product title"  />
                </div>
                <div class="col-sm-5 col-md-5">
                    <div class="">
                        <img class="img-responsive" src="" alt="">
                    </div>
                </div>
                <div class="form-group">     
                    <label for="inputDescription">Update Description</label>
                    <textarea name="newDescription" class="form-control" id="inputDescription" rows="3" placeholder="Product description..."></textarea>
                </div>
                <div class="form-group">
                    <label for="inputPrice">New Price</label>
                    <input type="number" name="newPrice" id="inputPrice" step="0.01" class="form-control" placeholder="New product price"  />
                </div>
                
                <div class="d-flex justify-content-end">
                  <input type="hidden" name="product_id" value="<?php echo $product->id; ?>" />  
                  <button name="btn_addToUpdate" class="btn btn-outline-primary">Update</button>
                </div>
         </form>

 
<?php include_once 'inc/footer.inc.php'; ?>