<?php
    require 'sec/users_only.php';
    require 'sec/admin_only.php';


require_once 'class/Category.class.php';
require_once 'class/Product.class.php';
require_once 'class/Helper.class.php';

$c = new Category();
$categories = $c->all();



if( isset($_POST['btn_addProduct']) ) {
  $newProduct = new Product();
  $newProduct->title = $_POST['title'];
  $newProduct->cat_id = $_POST['cat_id'];
  $newProduct->price = $_POST['price'];
  $newProduct->description = $_POST['description'];
  if( $newProduct->insert() ) {
    Helper::addMessage('Product added successfully.');
  }
}


?>

<?php include 'inc/header.inc.php'; ?>

<h1 class="my-5">Add Product</h1>

<form action="" method="post">
    
  <div class="form-row">

    <div class="form-group col-md-6">
      <label for="inputTitle">Title</label>
      <input type="text" name="title" class="form-control" id="inputTitle" placeholder="Product title"  />
    </div>

      <div class="form-group col-md-6">
      <label for="inputCategory">Category</label>
      <select name="cat_id" id="inputCategory" class="form-control">

        <?php foreach($categories as $category) { ?>
          <option value="<?php echo $category->id; ?>">
            <?php echo $category->title; ?>
          </option>
        <?php } ?>


      </select>
    </div>

  </div>

  <div class="form-row">

    <div class="form-group col-md-6">
      <label for="inputImage">Image</label>
      <input type="file" name="image" id="inputImage" class="form-control-file" />
    </div>

    <div class="form-group col-md-6">
      <label for="inputPrice">Price</label>
      <input type="number" name="price" id="inputPrice" step="0.01" class="form-control" placeholder="Product price"  />
    </div>

  </div>

  <div class="form-row">

    <div class="form-group col-md-12">
      <label for="inputDescription">Description</label>
      <textarea name="description" class="form-control" id="inputDescription" rows="3" placeholder="Product description..."></textarea>
    </div>

  </div>
<form action="" method="post">
  <div class="d-flex justify-content-end">
    <button name="btn_addProduct" class="btn btn-outline-primary">Add product</button>
  </div>

</form>

<?php include 'inc/footer.inc.php'; ?>