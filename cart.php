<?php

  require_once 'sec/users_only.php';
  require_once 'sec/admin_only.php';
  require_once 'class/User.class.php';
  require_once 'class/Product.class.php';
  require_once 'class/Helper.class.php';
  $u = new User();


  $products = $u->getCart();
  
  if(isset($_POST['btn_updateQuantity'])){
      $updateCart = new Product($_POST['cart_id']);
      $updateCart->quantity = $_POST['new_quantity'];
      $updateCart->updateQuantity($_POST['new_quantity']);
      
  }
  
  
  if(isset($_POST['btn_removeFromCart'])){
      $deleteCart = new Product($_POST['cart_id']);
      $deleteCart->deleteCart();
  }
  
?>
<?php include 'inc/header.inc.php'; ?>

<h1 class="my-5">Cart</h1>

<table class="table">

  <thead>
    <tr>
      <th>Product title</th>
      <th>Quantity</th>
      <th>Price</th>
      <th>Total price</th>
      <th>Actions</th>
    </tr>
  </thead>

  <tbody>
  <?php foreach($products as $product) { ?>
    <tr>
      <th><?php echo $product->title; ?></th>
      <td>
        <form action="" method="post">
          <div class="input-group input-group-sm">
            <input type="number" name="new_quantity" class="form-control" value="<?php echo $product->quantity; ?>" min="1" placeholder="Enter new quantity" />
            <input type="hidden" name="cart_id" value="<?php echo $product->id; ?>" />
            <div class="input-group-append">
                <input type="hidden" name="cart_id" value="<?php echo $product->id; ?>" />
              <button name="btn_updateQuantity" class="btn btn-outline-primary">Update</button>
            </div>
          </div>
        </form>
      </td>
      <td><?php echo $product->price; ?> RSD</td>
      <td><?php echo $product->quantity * $product->price; ?> RSD</td>
      <td>
        <form action="./cart.php" method="post">
          <input type="hidden" name="cart_id" value="<?php echo $product->id; ?>" />
          <button name="btn_removeFromCart" class="btn btn-outline-danger btn-sm">Remove from cart</button>
        </form>
      </td>
    </tr>
    <?php } ?>

  </tbody>

</table>

<?php include 'inc/footer.inc.php'; ?>