<?php
    require_once 'class/User.class.php';
    require_once 'class/Helper.class.php';

if(isset($_POST['btn_addToUpdateUser'])){
    $updateUser = new User($_POST['id']);
    $updateUser->name = $_POST['UpdateName'];
    $updateUser->username = $_POST['UpdateUsername'];
    $updateUser->email = $_POST['UpdateEmail'];
    $updateUser->password = $_POST['Updatepassword'];
    if( $updateUser->update()){
        Helper::User("User is updated");
    }
}


include_once 'inc/header.inc.php';
?>

<h1 class="my-5">Update profile</h1>


<form action="" method="post">
  <div class="form-group">
    <label for="inputUpdateName">Update Name</label>
    <input type="text" name="UpdateName" class="form-control" id="inputUpdateName"  placeholder="Enter Name">
  </div>
  <div class="form-group">
    <label for="inputUpdateUsername">Update Username</label>
    <input type="text" name="UpdateUsername" class="form-control" id="inputUpdateUsername" placeholder="Enter Username">
  </div>
  <div class="form-group">
    <label for="inputUpdateEmail">Update Email</label>
    <input type="email" name="UpdateEmail" class="form-control" id="inputUpdateEmail" placeholder="Enter Email address">
  </div>
  <div class="form-group">
    <label for="inputUpdatePassword">Update Password</label>
    <input type="password" name="Updatepassword" class="form-control" id="inputUpdatePassword" placeholder="Enter Password">
  </div>
    <div class="d-flex justify-content-end">
        <input type="hidden" name="id" value="<?php echo $user->id; ?>" />  
        <button name="btn_addToUpdateUser" class="btn btn-outline-primary">Update</button>
    </div>
</form>










<?php include_once 'inc/footer.inc.php'; ?>