<?php


require_once 'class/User.class.php';
require_once 'class/Helper.class.php';

$_loggedInUser2 = new User();
$_loggedInUser2->isUserLoggedIn();

if( !User::isUserLoggedIn() ) {
  Helper::addError('You have to login to access requested page.');
  header("Location: ./login.php");
  die();
}
